FROM wordpress
ADD $PWD/ports.conf /tmp/ports.conf
ADD $PWD/uploads.ini /usr/local/etc/php/conf.d/uploads.ini
RUN cp /tmp/ports.conf /etc/apache2/ports.conf
EXPOSE 8080
#ENTRYPOINT ["docker-entrypoint.sh"]
#USER www-data
#CMD echo 'hello word!!' && wp shell
# ENTRYPOINT /apache2_copy_theme.sh && apache2-foreground
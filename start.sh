# /bin/bash
docker run --name=wordpress \
   --link mymysql\
   --rm liqixin/wordpress \
    -e WORDPRESS_DB_PASSWORD=123456 \
    -e  WORDPRESS_DB_HOST=mymysql:3306 \
    -e WORDPRESS_DB_USER=root \
    -p 8080:8080 \
    -ti\
    liqixin/wordpress
